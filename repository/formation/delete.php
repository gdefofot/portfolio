<?php
    session_start();
    if(!isset($_SESSION['email'])){
        header("Location:../../pages/connexion.php");
    }
    
    include_once '../../config/database.php';
    if(isset($_GET['id'])){
        $sqlRe = "DELETE FROM formation WHERE id = :id";
        try{
            $req = $connexion->prepare($sqlRe);
            $req->execute(array(id=> $_GET['id']));
            echo "The record deleted successfully";
            header("Location:../../accueil#formations");
        } catch(PDOException $e) {
            echo $sqlRe . "<br>" . $e->getMessage();
        }
    }

?>