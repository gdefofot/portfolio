<?php
    session_start();
    if(!isset($_SESSION['email'])){
        header("Location:../../login");
    }
    
    if(isset($_POST['submit'])){
        $cheminImages = '../../assets/img/formation/';
        include '../../config/functions.php';
        
        $nomFichier = '';
        if($_FILES["logoUpload"]["size"] > 0){
            echo 'ici';
            $fichier = uploadFichier($cheminImages);
            if($fichier[0] === 1){
                $nomFichier = $fichier[1];
            }
        }else{
            $nomFichier = $_POST['logo'];
        }

        include '../../config/database.php';
        $sqlRe = "UPDATE formation SET nomFormation = :nomFormation, ecole = :ecole, anneeDiplome = :anneeDiplome, logo = :logo WHERE id= :id";
        try{
            $req = $connexion->prepare($sqlRe);
            $req->execute(array(
                ':nomFormation' => $_POST['nomFormation'], ':ecole' => $_POST['ecole'], ':anneeDiplome' => $_POST['anneeDiplome'],
                ':logo' => $nomFichier, ':id' => $_POST['id']));
            header("Location:../../accueil#formations");
        } catch(PDOException $e) {
            echo $sqlRe . "<br>" . $e->getMessage();
        }
    }
?>