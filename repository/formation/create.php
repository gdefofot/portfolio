<?php
    session_start();
    if(!isset($_SESSION['email'])){
        header("Location:../../login");
    }

    if(isset($_POST["submit"])) {
        if(!isset($_POST["description"])){
            $description = "";
        }
        $cheminImages = '../../assets/img/formation/';
        include '../../config/functions.php';
        $fichier = uploadFichier($cheminImages);
        if($fichier[0] === 1){
            include_once '../../config/database.php';
            $sqlRe = "INSERT INTO formation(nomFormation, ecole, anneeDiplome, logo) 
                    VALUES(:nomFormation, :ecole, :anneeDiplome, :logo)";
            try{
                $req = $connexion->prepare($sqlRe);
                $req->execute(array(':nomFormation' => $_POST['nomFormation'], ':ecole' => $_POST['ecole'], 
                                    ':anneeDiplome' => $_POST['anneeDiplome'],
                                    ':logo' => $fichier[1])
                );
                echo "Requête de création de formation OK";
                header("Location:../../accueil#formations");
            } catch(PDOException $e) {
                echo $sqlRe . "<br>" . $e->getMessage();
            }
        }
    }


