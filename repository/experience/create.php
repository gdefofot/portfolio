<?php
    session_start();
    if(!isset($_SESSION['email'])){
        header("Location:../../login");
    }

    if(isset($_POST["submit"])) {
        $cheminImages = '../../assets/img/experience/';
        include '../../config/functions.php';
        $fichier = uploadFichier($cheminImages);
        if($fichier[0] === 1){
            include_once '../../config/database.php';
            $sqlRe = "INSERT INTO experience(posteOccupe, nomEntreprise, dateDebut, dateFin, description, realisation, environnementTechnique, logo) 
                    VALUES(:posteOccupe, :nomEntreprise, :dateDebut, :dateFin, :description, :realisation, :environnementTechnique, :logo)";
            try{
                $req = $connexion->prepare($sqlRe);
                $req->execute(array(':posteOccupe' => $_POST['posteOccupe'], ':nomEntreprise' => $_POST['nomEntreprise'], 
                                    ':dateDebut' => $_POST['dateDebut'], ':dateFin' => $_POST['dateFin'],
                                    ':description' => $_POST['description'], ':realisation' => $_POST['realisation'], 
                                    ':environnementTechnique' => $_POST['environnementTechnique'], ':logo' => $fichier[1]
                    ));

                echo "Requête de création d'expérience OK";
                header("Location:../../accueil#experiences");
            } catch(PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }
        }
    }

?>
