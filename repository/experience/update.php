<?php

session_start();
if(!isset($_SESSION['email'])){
    header("Location:../../login");
}

if(isset($_POST['submit'])){
    $cheminImages = '../../assets/img/experience/';
    include '../../config/functions.php';
    
    $nomFichier = '';
    if($_FILES["logoUpload"]["size"] > 0){
        echo 'ici';
        $fichier = uploadFichier($cheminImages);
        if($fichier[0] === 1){
            $nomFichier = $fichier[1];
        }
    }else{
        $nomFichier = $_POST['logo'];
    }

    include '../../config/database.php';
    $sqlRe = "UPDATE experience SET posteOccupe= :posteOccupe, nomEntreprise = :nomEntreprise, dateDebut = :dateDebut,
        dateFin = :dateFin, description = :description, realisation = :realisation, environnementTechnique = :environnementTechnique, logo =:logo WHERE id= :id";
    try{
        $req = $connexion->prepare($sqlRe);
        $req->execute(array(
            ':posteOccupe' => $_POST['posteOccupe'], ':nomEntreprise' => $_POST['nomEntreprise'], 
            ':dateDebut' => $_POST['dateDebut'], ':dateFin' => $_POST['dateFin'],
            ':description' => $_POST['description'], ':realisation' => $_POST['realisation'], 
            ':environnementTechnique' => $_POST['environnementTechnique'], ':logo' => $nomFichier, ':id' => $_POST['id']
        ));
        header("Location:../../accueil#experiences");
    } catch(PDOException $e) {
        echo $sqlRe . "<br>" . $e->getMessage();
    }
}
?>