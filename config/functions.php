<?php
function uploadFichier($target_dir){
    $target_file = $target_dir . basename($_FILES["logoUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $resUpload ='';

    // Check if image file is a actual image or fake image
    
    $check = getimagesize($_FILES["logoUpload"]["tmp_name"]);
    if($check !== false) {
        echo "Le fichier est une image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        $resUpload = "Le fichier n'est pas une image.";
        $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["logoUpload"]["size"] > 500000) {
        $resUpload = "Désolé, le fichier est trop lourd.";
        $uploadOk = 0;
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        $resUpload = "Désolé, seuls les fichiers JPG, JPEG, PNG & GIF sont autorisés.";
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $resUpload = "Désolé, le fichier n'a pas été téléchargé.";
        return [0, $resUpload];
    // if everything is ok, try to upload file
    } else {
            // Check if file already exists
        if (file_exists($target_file)) {
            $resUpload = htmlspecialchars( basename( $_FILES["logoUpload"]["name"]));
            return [1, $resUpload];
        }
        else if (move_uploaded_file($_FILES["logoUpload"]["tmp_name"], $target_file)) {
            $resUpload = htmlspecialchars( basename( $_FILES["logoUpload"]["name"]));
            return [1, $resUpload];
        } else {
            $resUpload = "Désolé, une erreur est survenue pendant le chargement.";
            return [0, $resUpload];
        }
    }
}

?>