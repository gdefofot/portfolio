<?php
    $url='localhost:8889';
    $dbUsername='defofotg';
    $dbPassword='defofotg';
    $dbName='projet_php';


    try{
        $connexion = new PDO('mysql:host='.$url.';dbname='.$dbName.';charset=utf8',$dbUsername,$dbPassword);
        $connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(Exception $e){
        die('Could not Connect My Sql because:' . $e ->getMessage());
    }
?>