<?php

// Check for empty fields
if(empty($_POST['name']) || empty($_POST['email']) || empty($_POST['phone']) || empty($_POST['message']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
  error_log($_POST['email']);
  error_log(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL));
  http_response_code(500);
  exit();
}

require_once '../../config/PHPMailer/PHPMailerAutoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Username = 'portfolio@defogeorges.fr';            // SMTP username
$mail->Password = 'zA4-D*c_U1';                           // SMTP password
$mail->Host = 'mail46.lwspanel.com';                       // Specify main and backup SMTP servers
$mail->Port = 465;                                    // TCP port to connect to
$mail->setFrom('portfolio@defogeorges.fr');
$mail->isHTML(true);                                  // Set email format to HTML


$name = strip_tags(htmlspecialchars($_POST['name']));
$email = strip_tags(htmlspecialchars($_POST['email']));
$phone = strip_tags(htmlspecialchars($_POST['phone']));
$message = strip_tags(htmlspecialchars($_POST['message']));

$mail->Subject = 'Contact depuis CV ';
$mail->Body    = "
Mail venant de notre CV en ligne.\n\n".
"Voici les détails:\n\n".
"Nom: $name\n\n".
"Email: $email\n\n".
"Téléphone: $phone\n\n".
"Message: $message";

$mail->addAddress('portfolio@defogeorges.fr');

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
    http_response_code(500);
} else {
    echo 'Message has been sent';
}
?> 
