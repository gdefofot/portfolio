<?php 

session_start();

if (isset($_POST['email']) && isset($_POST['password'])){
    $email = $_POST['email'];
    $password = $_POST['password'];

    include_once '../config/database.php';

    $sql = 'SELECT * FROM utilisateur WHERE email = :email';

    try{
        $requete = $connexion -> prepare($sql);
        $requete -> execute(array(':email'=>$email));
    }
    catch(PDOException $e)
    {
        throw new Exception('Erreur de récupération des données : '. $e->getMessage());
    }
    
    $user = $requete->fetch(PDO::FETCH_ASSOC);
    if(is_array($user) && password_verify($password, $user['password'])){
        $_SESSION['email'] = $email;
        header("Location:accueil");
    }else {
        header("Location:login?auth=false");
    }
}
?>