<?php 
    session_start();
    if(!isset($_SESSION['email'])){
        header("Location:../../pages/connexion.php");
    }
    $titre = "Modifier une expérience";
    $cheminCss = "../../css/styles.css";
    $cheminMainCss = "../../css/main.css";
    $logo = "../../assets/img/logo.gif";
    $pageAccueil = "accueil";
    $pageConnexion = "login";
    $pageDeconnexion = "../../authentification/deconnexion.php";
    $ancreExperiences = "../../accueil#experiences";
    $ancreFormations = "../../accueil#formations";
    $ancreContact = "../../accueil#contact";

    if(isset($_GET['id'])){
        include '../../config/database.php';
        $sqlRe = "SELECT * FROM experience WHERE id= :id";
        try{
            $req = $connexion->prepare($sqlRe);
            $req->execute(array(":id" => $_GET['id']));
            $experience = $req->fetch();
            $req->closeCursor();
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
    }
?>

<!DOCTYPE html>
<html lang="fr">
    <?php include '../../includes/head.php'?>
    <body id="page-top" class="espace-nav text-center font-weight-bold">
        <!-- Navigation-->
        <?php include '../../includes/nav.php'?>
        <div class="container mb-5">
            <div class="row justify-content-center">
                <div class = col-lg-12>
                    <div class="row">
                        <!-- Portfolio Modal - Title-->
                        <h3 class="col-lg-12 portfolio-modal-title text-secondary text-uppercase mb-0">Modifier une expérience</h3>
                    </div>
                </div>
                <div class="col-lg-8">                
                    <!-- Icon Divider-->
                    <div class="divider-custom">
                        <div class="divider-custom-line"></div>
                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                        <div class="divider-custom-line"></div>
                    </div>

                    <!-- Portfolio Modal - Text-->
                    <form class="formulaire_ajout text-left" action="../../repository/experience/update.php" method="post" enctype="multipart/form-data">
                        <input type="text" class="form-control" name="id" value="<?php echo $experience['id'];?>" required hidden>

                        <label for="posteOccupe">Poste occupé</label>
                        <input type="text" id="posteOccupe" class="form-control" name="posteOccupe" value="<?php echo $experience['posteOccupe'];?>" required autofocus>

                        <label for="nomEntreprise">Nom de l'entreprise</label>
                        <input type="text" id="nomEntreprise" class="form-control" name="nomEntreprise" value="<?php echo $experience['nomEntreprise'];?>" required>

                        <label for="dateDebut">Date de début</label>
                        <input type="text" id="dateDebut" class="form-control" name="dateDebut" value="<?php echo $experience['dateDebut'];?>" placeholder="mm-aaaa" required>

                        <label for="dateFin">Date de fin</label>
                        <input type="text" id="dateFin" class="form-control" name="dateFin" value="<?php echo $experience['dateFin'];?>" placeholder="mm-aaaa" required>

                        <label for="description">Description</label>
                        <textarea id="description" class="form-control" name="description" required><?php echo htmlspecialchars($experience['description']); ?></textarea>

                        <label for="realisation">Réalisation</label>
                        <textarea id="realisation" class="form-control" name="realisation" required><?php echo htmlspecialchars($experience['realisation']); ?></textarea>

                        <label for="environnementTechnique">Environnement Technique</label>
                        <textarea id="environnementTechnique" class="form-control" name="environnementTechnique" required><?php echo htmlspecialchars($experience['environnementTechnique']); ?></textarea>

                        <label for="logo">Logo</label>
                        <input type="file" name="logoUpload" id="logo" class="form-control" placeholder="nom_image.png">
                        <input type="text" name="logo" class="form-control" value="<?php echo $experience['logo'];?>" hidden>

                        <div class="text-right">
                            <button class="btn btn-primary mt-4" type="submit" name="submit">
                                <i class="far fa-save fa-fw"></i>
                                Enregistrer</button>
                            <a class="btn btn-primary ml-2 mt-4" href="../../accueil">
                                <i class="fas fa-times fa-fw"></i>
                                Annuler
                            </a>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
        <!-- Footer-->
        <?php include '../../includes/footer.php'?>
    </body>
</html>