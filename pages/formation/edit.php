<?php 
    session_start();
    if(!isset($_SESSION['email'])){
        header("Location:../../pages/connexion.php");
    }
    $titre = "Modifier une formation";
    $cheminCss = "../../css/styles.css";
    $cheminMainCss = "../../css/main.css";
    $logo = "../../assets/img/logo.gif";
    $pageAccueil = "accueil";
    $pageConnexion = "login";
    $pageDeconnexion = "../../authentification/deconnexion.php";
    $ancreExperiences = "../../accueil#experiences";
    $ancreFormations = "../../accueil#formations";
    $ancreContact = "../../accueil#contact";

    if(isset($_GET['id'])){
        include '../../config/database.php';
        $sqlRe = "SELECT * FROM formation WHERE id= :id";
        try{
            $req = $connexion->prepare($sqlRe);
            $req->execute(array(":id" => $_GET['id']));
            $formation = $req->fetch();
            $req->closeCursor();
        } catch(PDOException $e) {
            echo $sql . "<br>" . $e->getMessage();
        }
    }
?>

<!DOCTYPE html>
<html lang="fr">
    <?php include '../../includes/head.php'?>
    <body id="page-top" class="espace-nav text-center font-weight-bold">
        <!-- Navigation-->
        <?php include '../../includes/nav.php'?>
        <div class="container mb-5">
            <div class="row justify-content-center">
                <div class = col-lg-12>
                    <div class="row">
                        <!-- Portfolio Modal - Title-->
                        <h3 class="col-lg-12 portfolio-modal-title text-secondary text-uppercase mb-0">Modifier une formation</h3>
                    </div>
                </div>
                <div class="col-lg-8">                
                    <!-- Icon Divider-->
                    <div class="divider-custom">
                        <div class="divider-custom-line"></div>
                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                        <div class="divider-custom-line"></div>
                    </div>

                    <!-- Portfolio Modal - Text-->
                    <form class="formulaire_ajout text-left" action="../../repository/formation/update.php" method="post" enctype="multipart/form-data">
                        <input type="text" class="form-control" name="id" value="<?php echo $formation['id'];?>" required hidden>

                        <label for="nomFormation">Nome de la formation</label>
                        <input type="text" id="nomFormation" class="form-control" name="nomFormation" value="<?php echo $formation['nomFormation'];?>" required autofocus>

                        <label for="ecole">Nom de l'école</label>
                        <input type="text" id="ecole" class="form-control" name="ecole" value="<?php echo $formation['ecole'];?>" required>

                        <label for="anneeDiplome">Année du diplôme</label>
                        <input type="text" id="anneeDiplome" class="form-control" name="anneeDiplome" placeholder="aaaa" value="<?php echo $formation['anneeDiplome'];?>" required>

                        <label for="logo">Logo</label>
                        <input type="file" name="logoUpload" id="logo" class="form-control" placeholder="nom_image.png">
                        <input type="text" name="logo" class="form-control" value="<?php echo $formation['logo'];?>" hidden>
                        
                        <div class="text-right">
                            <button class="btn btn-primary mt-4" type="submit" name="submit">
                                <i class="far fa-save fa-fw"></i>
                                Enregistrer</button>
                            <a class="btn btn-primary ml-2 mt-4" href="../../accueil">
                                <i class="fas fa-times fa-fw"></i>
                                Annuler
                            </a>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
        <!-- Footer-->
        <?php include '../../includes/footer.php'?>
    </body>
</html>