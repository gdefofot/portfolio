<?php 
    session_start();
    if(isset($_SESSION['email'])){
        header("Location:../accueil");
    }
    $titre = "Connexion";
    $cheminCss = "css/styles.css";
    $cheminMainCss = "css/main.css";
    $logo = "assets/img/logo.gif";
    $pageAccueil = "accueil";
    $pageConnexion = "login";
    $pageDeconnexion = "../authentification/deconnexion.php";
    $ancreExperiences = "accueil#experiences";
    $ancreFormations = "accueil#formations";
    $ancreContact = "accueil#contact";
?>
<!DOCTYPE html>
<html lang="en">
    <?php include '../includes/head.php'?>
    <body id="page-top" class="connexion text-center">
        <!-- Navigation-->
        <?php include '../includes/nav.php'?>
        <form class="form-signin" action="login-action" method="post">
            <img class="mb-4" src="https://getbootstrap.com/docs/4.0/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Connectez-vous</h1>
            
            <label for="inputEmail" class="sr-only">Adresse e-mail</label>
            <input type="email" id="inputEmail" class="form-control" placeholder="email" name="email" required autofocus>
            
            <label for="inputPassword" class="sr-only">Mot de passe</label>
            <input type="password" id="inputPassword" class="form-control" placeholder="mot de passe" name="password" required>

            <button class="btn btn-lg btn-primary btn-block" type="submit">connexion</button>
            <?php 
                if(isset($_GET['auth'])){
                    echo '<div class="alert alert-danger mt-2" role="alert">Connexion échouée vérifiez votre adresse mail ou mot de passe.</div>';
                } 
            ?>
            
        </form>
        <!-- Footer-->
        <?php include '../includes/footer.php'?>
    </body>
</html>