<?php 
    $experiences = $connexion->query("SELECT * FROM experience");
?>
<section class="page-section portfolio" id="experiences">
    <div class="container">
        <!-- Portfolio Section Heading-->
        <h2 class="page-section-heading text-center text-uppercase text-secondary mb-0">Expériences</h2>
        <!-- Icon Divider-->
        <div class="divider-custom">
            <div class="divider-custom-line"></div>
            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
            <div class="divider-custom-line"></div>
        </div>
        <!-- Portfolio Grid Items-->
        <div class="row justify-content-center">
            <?php 
                if ($experiences->rowCount() > 0) {
                    while($experience = $experiences->fetch()){
                        echo '
                        <div class="col-md-6 col-lg-4 mb-5">
                            <div class="portfolio-item mx-auto" data-toggle="modal" data-target="#portfolioModal'.$experience['id'].'">
                                <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                    <div class="portfolio-item-caption-content text-center text-white"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid logo-experience" src="assets/img/experience/'.$experience['logo'].'" alt="" />
                                <div class="text-center font-weight-bold">'.$experience['posteOccupe'].'</div>
                            </div>
                        </div>';
                    }
                    if(isset($_SESSION['email'])){
                        echo '<div class="col-md-6 col-lg-4 mb-5">
                                <a class="portfolio-item mx-auto" href="add/experience/" id="btn-ajout-exp">
                                    <img class="img-fluid logo-experience" id="bouton-ajout" src="assets/img/experience/add.png" alt="" />
                                    <div class="text-center font-weight-bold">Ajouter une expérience</div>
                                </a>
                            </div>';
                    }
                } else {
                    echo '<div class="no-data"><strong>Aucune expérience pour le moment.</strong></div>';
                }
            ?>
        </div>
    </div>
</section>