<?php 
    $experiences = $connexion->query("SELECT * FROM experience");
    if ($experiences->rowCount() > 0) {
        while($experience = $experiences->fetch()){
            echo 
            '<div class="portfolio-modal modal fade" id="portfolioModal'.$experience['id'].'" tabindex="-1" role="dialog" aria-labelledby="portfolioModal'.$experience['id'].'Label" aria-hidden="true">
                <div class="modal-dialog modal-xl" role="document">
                    <div class="modal-content">
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"><i class="fas fa-times"></i></span>
                        </button>
                        <div class="modal-body text-center">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class = col-lg-12>
                                        <div class="row">
                                            <div class="col-lg-2">
                                                <!-- Portfolio Modal - Image-->
                                                <img class="img-fluid rounded mb-2" src="assets/img/experience/'.$experience['logo'].'" alt="" />
                                            </div>
                                            <!-- Portfolio Modal - Title-->
                                            <h2 class="col-lg-10 portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal'.$experience['id'].'Label">'.$experience['posteOccupe'].'</h2>
                                        </div>

                                        <div class="row">
                                            <div class=" col-lg-12 text-center font-weight-bold">
                                                <h3>'.$experience['nomEntreprise'].'</h3>
                                                <div>'.$experience['dateDebut'].' / '.$experience['dateFin'].'</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">                
                                        <!-- Icon Divider-->
                                        <div class="divider-custom">
                                            <div class="divider-custom-line"></div>
                                            <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                            <div class="divider-custom-line"></div>
                                        </div>

                                        <!-- Portfolio Modal - Text-->
                                        <h3 class="text-secondary text-uppercase my-4">Contexte</h3>
                                        <div class="text-justify">'.$experience['description'].'</div>
                                                                                
                                        <h3 class="text-secondary text-uppercase my-4">Réalisation</h3>
                                        <div class="text-justify">'.$experience['realisation'].'</div>

                                        <h3 class="text-secondary text-uppercase my-4">Environnement technique</h3>
                                        <div class="text-justify">'.$experience['environnementTechnique'].'</div>';
                                        
                                        if(isset($_SESSION['email'])){
                                            echo '
                                                <div class="d-flex justify-content-around mt-5">
                                                    <a class="btn btn-info btn-edit-exp mt-4" href="edit/experience/'.$experience['id'].'">
                                                        <i class="fas fa-edit fa-fw"></i>
                                                        Modifier
                                                    </a>
                                                    <a class="btn btn-danger btn-delete-exp mt-4" href="repository/experience/delete.php?id='.$experience['id'].'">
                                                        <i class="fas fa-trash-alt fa-fw"></i>
                                                        Supprimer
                                                    </a>';
                                        }
                                        
                                        echo '
                                            <button class="btn btn-secondary ml-2 mt-4" data-dismiss="modal">
                                                <i class="fas fa-times fa-fw"></i>
                                                Fermer
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
        }
    }

    $formations = $connexion->query("SELECT * FROM formation");
    if ($formations->rowCount() > 0) {
        while($formation = $formations->fetch()){
            echo 
            '<div class="portfolio-modal modal fade formation-modal" id="formationModal'.$formation['id'].'" tabindex="-1" role="dialog" aria-labelledby="formationModal'.$formation['id'].'Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class = col-lg-10 justify-content-center>
                                        <!-- Portfolio Modal - Title-->
                                        <h3 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="formationModal'.$formation['id'].'Label">'.$formation['nomFormation'].'</h3>
                                </div>
                                <div class="col-lg-8">                
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
        
                                    <div class="row align-items-center">
                                        <div class="col-lg-3">
                                            <!-- Portfolio Modal - Image-->
                                            <img class="img-fluid rounded mb-2" src="assets/img/formation/'.$formation['logo'].'" alt="" />
                                        </div>
                                        <div class=" col-lg-9 text-center font-weight-bold">
                                            <h3>'.$formation['ecole'].'</h3>
                                            <div>'.$formation['anneeDiplome'].'</div>
                                        </div>
                                    </div>';
                                    
                                    if(isset($_SESSION['email'])){
                                        echo '
                                            <div class="d-flex justify-content-around mt-5">
                                                <a class="btn btn-info btn-edit-exp mt-4" href="edit/formation/'.$formation['id'].'">
                                                    <i class="fas fa-edit fa-fw"></i>
                                                    Modifier
                                                </a>
                                                <a class="btn btn-danger btn-delete-exp mt-4" href="repository/formation/delete.php?id='.$formation['id'].'">
                                                    <i class="fas fa-trash-alt fa-fw"></i>
                                                    Supprimer
                                                </a>';
                                    }
                                    
                                    echo '
                                        <button class="btn btn-secondary ml-2 mt-4" data-dismiss="modal">
                                            <i class="fas fa-times fa-fw"></i>
                                            Fermer
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
        }
    }
?>



