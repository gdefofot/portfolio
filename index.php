<?php 
    session_start();
    include_once 'config/database.php';
    $titre = "Bienvenue sur mon CV";
    $cheminCss = "css/styles.css";
    $cheminMainCss = "css/main.css";
    $logo = "assets/img/logo.gif";
    $pageAccueil = "accueil";
    $pageConnexion = "login";
    $pageDeconnexion = "authentification/deconnexion.php";
    $ancreExperiences = "#experiences";
    $ancreFormations = "#formations";
    $ancreContact = "#contact";
?>
<!DOCTYPE html>
<html lang="en">
    <?php include 'includes/head.php'?>
    <body id="page-top">
        <!-- Navigation-->
        <?php include 'includes/nav.php'?>
        <!-- Masthead-->
        <?php include 'includes/presentation.php'?>
        <!-- Experiences Section-->
        <?php include 'includes/experiences.php'?>
        <!-- Formations Section-->
        <?php include 'includes/formations.php'?>
        <!-- Contact Section-->
        <?php include 'includes/contact.php'?>
        <!-- Footer-->
        <?php include 'includes/footer.php'?>
        <!-- Portfolio Modal -->
        <?php include 'includes/modal.php'?>
    </body>
</html>
